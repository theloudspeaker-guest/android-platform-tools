Description: Remove Nullable annotation
 Nullable annnotation is just a warning that hints you can pass null to
 methods that can take null arguments. Can be safely ignored since it
 otherwise fails the build as it comes from libcore itself,
 which we don't build.
Author: Raman Sarda <theloudspeaker@disroot.org>
Last-Update: 2020-05-18

--- a/libcore/json/src/main/java/org/json/JSONObject.java
+++ b/libcore/json/src/main/java/org/json/JSONObject.java
@@ -23,7 +23,6 @@ import java.util.LinkedHashMap;
 import java.util.Map;
 import java.util.Objects;
 import java.util.Set;
-import libcore.util.Nullable;
 
 // Note: this class was written without inspecting the non-free org.json sourcecode.
 
@@ -256,7 +255,7 @@ public class JSONObject {
      *     infinities}.
      * @return this object.
      */
-    public JSONObject put( String name, @Nullable Object value) throws JSONException {
+    public JSONObject put( String name, Object value) throws JSONException {
         if (value == null) {
             nameValuePairs.remove(name);
             return this;
@@ -273,7 +272,7 @@ public class JSONObject {
      * Equivalent to {@code put(name, value)} when both parameters are non-null;
      * does nothing otherwise.
      */
-    public JSONObject putOpt(@Nullable String name, @Nullable Object value) throws JSONException {
+    public JSONObject putOpt(String name, Object value) throws JSONException {
         if (name == null || value == null) {
             return this;
         }
@@ -300,7 +299,7 @@ public class JSONObject {
      */
     // TODO: Change {@code append) to {@link #append} when append is
     // unhidden.
-    public JSONObject accumulate( String name, @Nullable Object value) throws JSONException {
+    public JSONObject accumulate( String name, Object value) throws JSONException {
         Object current = nameValuePairs.get(checkName(name));
         if (current == null) {
             return put(name, value);
@@ -361,7 +360,7 @@ public class JSONObject {
      * @return the value previously mapped by {@code name}, or null if there was
      *     no such mapping.
      */
-    @Nullable public Object remove(@Nullable String name) {
+    public Object remove(String name) {
         return nameValuePairs.remove(name);
     }
 
@@ -369,7 +368,7 @@ public class JSONObject {
      * Returns true if this object has no mapping for {@code name} or if it has
      * a mapping whose value is {@link #NULL}.
      */
-    public boolean isNull(@Nullable String name) {
+    public boolean isNull(String name) {
         Object value = nameValuePairs.get(name);
         return value == null || value == NULL;
     }
@@ -378,7 +377,7 @@ public class JSONObject {
      * Returns true if this object has a mapping for {@code name}. The mapping
      * may be {@link #NULL}.
      */
-    public boolean has(@Nullable String name) {
+    public boolean has(String name) {
         return nameValuePairs.containsKey(name);
     }
 
@@ -399,7 +398,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name}, or null if no such mapping
      * exists.
      */
-    @Nullable public Object opt(@Nullable String name) {
+    public Object opt(String name) {
         return nameValuePairs.get(name);
     }
 
@@ -423,7 +422,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists and is a boolean or
      * can be coerced to a boolean, or false otherwise.
      */
-    public boolean optBoolean(@Nullable String name) {
+    public boolean optBoolean(String name) {
         return optBoolean(name, false);
     }
 
@@ -431,7 +430,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists and is a boolean or
      * can be coerced to a boolean, or {@code fallback} otherwise.
      */
-    public boolean optBoolean(@Nullable String name, boolean fallback) {
+    public boolean optBoolean(String name, boolean fallback) {
         Object object = opt(name);
         Boolean result = JSON.toBoolean(object);
         return result != null ? result : fallback;
@@ -457,7 +456,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists and is a double or
      * can be coerced to a double, or {@code NaN} otherwise.
      */
-    public double optDouble(@Nullable String name) {
+    public double optDouble(String name) {
         return optDouble(name, Double.NaN);
     }
 
@@ -465,7 +464,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists and is a double or
      * can be coerced to a double, or {@code fallback} otherwise.
      */
-    public double optDouble(@Nullable String name, double fallback) {
+    public double optDouble(String name, double fallback) {
         Object object = opt(name);
         Double result = JSON.toDouble(object);
         return result != null ? result : fallback;
@@ -491,7 +490,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists and is an int or
      * can be coerced to an int, or 0 otherwise.
      */
-    public int optInt(@Nullable String name) {
+    public int optInt(String name) {
         return optInt(name, 0);
     }
 
@@ -499,7 +498,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists and is an int or
      * can be coerced to an int, or {@code fallback} otherwise.
      */
-    public int optInt(@Nullable String name, int fallback) {
+    public int optInt(String name, int fallback) {
         Object object = opt(name);
         Integer result = JSON.toInteger(object);
         return result != null ? result : fallback;
@@ -528,7 +527,7 @@ public class JSONObject {
      * can be coerced to a long, or 0 otherwise. Note that JSON represents numbers as doubles,
      * so this is <a href="#lossy">lossy</a>; use strings to transfer numbers via JSON.
      */
-    public long optLong(@Nullable String name) {
+    public long optLong(String name) {
         return optLong(name, 0L);
     }
 
@@ -538,7 +537,7 @@ public class JSONObject {
      * numbers as doubles, so this is <a href="#lossy">lossy</a>; use strings to transfer
      * numbers via JSON.
      */
-    public long optLong(@Nullable String name, long fallback) {
+    public long optLong(String name, long fallback) {
         Object object = opt(name);
         Long result = JSON.toLong(object);
         return result != null ? result : fallback;
@@ -563,7 +562,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists, coercing it if
      * necessary, or the empty string if no such mapping exists.
      */
-    public String optString(@Nullable String name) {
+    public String optString(String name) {
         return optString(name, "");
     }
 
@@ -571,7 +570,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists, coercing it if
      * necessary, or {@code fallback} if no such mapping exists.
      */
-    public String optString(@Nullable String name, String fallback) {
+    public String optString(String name, String fallback) {
         Object object = opt(name);
         String result = JSON.toString(object);
         return result != null ? result : fallback;
@@ -597,7 +596,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists and is a {@code
      * JSONArray}, or null otherwise.
      */
-    @Nullable public JSONArray optJSONArray(@Nullable String name) {
+    public JSONArray optJSONArray(String name) {
         Object object = opt(name);
         return object instanceof JSONArray ? (JSONArray) object : null;
     }
@@ -622,7 +621,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists and is a {@code
      * JSONObject}, or null otherwise.
      */
-    @Nullable public JSONObject optJSONObject(@Nullable String name) {
+    public JSONObject optJSONObject(String name) {
         Object object = opt(name);
         return object instanceof JSONObject ? (JSONObject) object : null;
     }
@@ -632,7 +631,7 @@ public class JSONObject {
      * array contains null for names that aren't mapped. This method returns
      * null if {@code names} is either null or empty.
      */
-    @Nullable public JSONArray toJSONArray(@Nullable JSONArray names) throws JSONException {
+    public JSONArray toJSONArray(JSONArray names) throws JSONException {
         JSONArray result = new JSONArray();
         if (names == null) {
             return null;
@@ -678,7 +677,7 @@ public class JSONObject {
      * Returns an array containing the string names in this object. This method
      * returns null if this object contains no mappings.
      */
-    @Nullable public JSONArray names() {
+    public JSONArray names() {
         return nameValuePairs.isEmpty()
                 ? null
                 : new JSONArray(new ArrayList<String>(nameValuePairs.keySet()));
@@ -761,7 +760,7 @@ public class JSONObject {
      * @param data the string to encode. Null will be interpreted as an empty
      *     string.
      */
-    public static String quote(@Nullable String data) {
+    public static String quote(String data) {
         if (data == null) {
             return "\"\"";
         }
@@ -788,7 +787,7 @@ public class JSONObject {
      * Otherwise if the object is from a {@code java} package, returns the result of {@code toString}.
      * If wrapping fails, returns null.
      */
-    @Nullable public static Object wrap(@Nullable Object o) {
+    public static Object wrap(Object o) {
         if (o == null) {
             return NULL;
         }
